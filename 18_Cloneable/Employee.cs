﻿using System;

namespace _18_Cloneable
{
    class Employee : Person, ICloneable, IMyCloneable<Employee>
    {
        public string Department { get; set; }
        public decimal Salary { get; set; }        
        
        public Employee(string name, int age, string phone, decimal salary, string departmentName) : base(name, age, phone)
        {            
            Department = departmentName;            
            Salary = salary;            
        }
        public override object Clone()
        {
            return this.GetPrototype();
        }

        public new Employee GetPrototype()
        {            
            Person person = base.GetPrototype();
            return new Employee(person.Name, person.Age, person.Phone, this.Salary, this.Department);
        }
    }    
}

