﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _18_Cloneable
{
    interface IMyCloneable<T> where T: class
    {
        T GetPrototype();
    }
}
