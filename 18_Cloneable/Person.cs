﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _18_Cloneable
{
    public class Person:ICloneable, IMyCloneable<Person>
    {
        public string  Name { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }


        public Person(string name, int age, string phone)
        {
            Name = name;
            Age = age;
            Phone = phone;
        }

        public virtual object Clone()
        {
            return this.GetPrototype();
        }       

        public Person GetPrototype()
        {
            return new Person(Name=this.Name, Age=this.Age, Phone=this.Phone);
        }

        public override string ToString()
        {
            return "Person: " + Name + ", Age: " + Age;
        }
    }












    //class Company : ICloneable
    //{
    //    public Company(string companyName)
    //    {
    //        CompanyName = companyName;
    //    }

    //    public string CompanyName { get; set; } = "PlantCity";

    //    public virtual object Clone()
    //    {
    //        return new Company(CompanyName);
    //    }
    //}



    //class Department : Company, ICloneable, IMyCloneable<Department>
    //{
    //    public string DepartmentName { get; set; }
    //    public Department(string companyName, string departmentName):base(companyName)   
    //    {            
    //        DepartmentName = departmentName;            
    //    }
    //    public override object Clone()
    //    {
    //        Company company = (Company)base.Clone();
    //        return new Department(company.CompanyName, DepartmentName);            
    //    }

    //    public Department GetPrototype()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}

