﻿using System;

namespace _18_Cloneable
{
    class Program
    {
        static void Main(string[] args)
        {
            Person firstPerson = new Person("David", 44, "798-005");
            Person secondPerson = (Person)firstPerson.Clone();

            Console.WriteLine(firstPerson.ToString());
            Console.WriteLine(firstPerson.GetHashCode());

            Console.WriteLine(secondPerson.ToString());
            Console.WriteLine(secondPerson.GetHashCode());         
            
            
            
            SecurityOfficer securityMan = new SecurityOfficer("John Scott", 45, "345-765", 4000, 2);
            Console.WriteLine(securityMan.ToString());
            Console.WriteLine(securityMan.GetHashCode());

            SecurityOfficer securitySecondMan = securityMan.GetPrototype();
            Console.WriteLine(securitySecondMan.ToString());
            Console.WriteLine(securitySecondMan.GetHashCode());

            Console.ReadLine();
        }
    }
}
