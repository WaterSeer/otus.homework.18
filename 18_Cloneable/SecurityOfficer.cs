﻿using System;

namespace _18_Cloneable
{
    
    class SecurityOfficer : Employee, ICloneable, IMyCloneable<SecurityOfficer>
    {
        byte SecurityLevel { get; set; }
        public SecurityOfficer(string name, int age, string phone, decimal salary, byte securityLevel):base(name,age,phone,salary,departmentName: "SecurityDepartment")
        {
            SecurityLevel = securityLevel;
        } 

        public new SecurityOfficer GetPrototype()
        {
            Employee employee = base.GetPrototype();
            return new SecurityOfficer(employee.Name, employee.Age, employee.Phone, employee.Salary, this.SecurityLevel);             
        }

        public new object Clone()
        {
            return this.GetPrototype();
        }

        public override string ToString()
        {
            return "Name: " + this.Name + "  Age: " + this.Age + "  Security Level: " + Enum.GetName(typeof(SecurityLevel), this.SecurityLevel);
        }
    }    
}

